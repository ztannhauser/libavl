#include <stdio.h>
#include <stdlib.h>

#include "../structs.h"

#define max(x, y) (x > y ? x : y)

void private_avl_update_height(struct avl_node* node)
{
	int lh = node->left ? node->left->height : 0;
	int rh = node->right ? node->right->height : 0;
	node->height = max(lh, rh) + 1;
}

