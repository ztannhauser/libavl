struct avl_node
{
	struct avl_node* next;
	struct avl_node* prev;
	struct avl_node* parent;
	struct avl_node* left;
	struct avl_node* right;
	int height;
	union
	{
		void* data;
		void* item;
	};
};

struct avl_tree
{
	struct avl_node* root;
	struct avl_node* leftmost;
	struct avl_node* rightmost;
	unsigned long n;
	int (*compare)(const void* a, const void* b);
	void (*free)(void* a);
};
