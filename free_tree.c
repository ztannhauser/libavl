#include <stdio.h>
#include <stdlib.h>

#include "./structs.h"
#include "./free_nodes.h"

void avl_free_tree(struct avl_tree* this)
{
	avl_free_nodes(this);
	free(this);
}

