
struct avl_tree* avl_alloc_tree(
	int (*compare)(const void* a, const void* b),
	void (*free)(void* a));
