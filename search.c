#include <stdio.h>
#include <stdlib.h>

#include "./structs.h"

struct avl_node* avl_tree_search(
	struct avl_tree* this,
	int (*compare)(const void*, const void*),
	const void* b)
{
	struct avl_node* ret = this->root;
	int c;
	while(ret && ((c = compare(b, ret->data))))
	{
		if(c > 0)
		{
			ret = ret->right;
		}
		else
		{
			ret = ret->left;
		}
	}
	return ret;
}

