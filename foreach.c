
#include "./structs.h"

void avl_foreach(struct avl_tree* this, void callback(void* data))
{
	for(
		struct avl_node* i = this->leftmost;
		i; i = i->next)
	{
		callback(i->data);
	}
}
