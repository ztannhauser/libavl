
struct avl_tree* avl_init_tree(
	struct avl_tree* this,
	int (*compare)(const void* a, const void* b),
	void (*free)(void* a));
