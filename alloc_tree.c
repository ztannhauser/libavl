#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "./structs.h"

#include "./init_tree.h"

struct avl_tree* avl_alloc_tree(
	int (*compare)(const void* a, const void* b),
	void (*free)(void* a))
{
	assert(compare);
	return avl_init_tree(
		malloc(sizeof(struct avl_tree)),
		compare,
		free);
}

